<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class InputTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ([
            [
                'name' => 'url'
            ],
            [
                'name' => 'text'
            ],
            [
                'name' => 'textarea'
            ],
            [
                'name' => 'phone'
            ],
            [
                'name' => 'email'
            ],
            [
                'name' => 'color'
            ],
            [
                'name' => 'file'
            ]
        ] as $service) {
            $service['created_at'] = Carbon::now();
            $service['updated_at'] = Carbon::now();
            DB::table('input_types')->insert($service);
        }
    }
}
