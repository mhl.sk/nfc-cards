<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ([
            [
                'name' => 'email',
                'label' => 'E-Mail',
                'type_id' => 5
            ],
            [
                'name' => 'phone',
                'label' => 'Phone',
                'type_id' => 4
            ],[
                'name' => 'linkedin',
                'label' => 'LinkedIn',
                'type_id' => 1
            ],
            [
                'name' => 'website',
                'label' => 'Website',
                'type_id' => 1
            ],
            [
                'name' => 'youtube',
                'label' => 'YouTube',
                'type_id' => 1
            ],
            [
                'name' => 'twitter',
                'label' => 'Twitter',
                'type_id' => 1
            ],
            [
                'name' => 'instagram',
                'label' => 'Instagram',
                'type_id' => 1
            ],
            [
                'name' => 'tiktok',
                'label' => 'TikTok',
                'type_id' => 1
            ]
         ] as $service) {
            $service['created_at'] = Carbon::now();
            $service['updated_at'] = Carbon::now();
            DB::table('services')->insert($service);
        }
    }

}
