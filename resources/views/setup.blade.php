<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Setup a card') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="shadow sm:rounded-lg">
                <div class="bg-white overflow-hidden sm:rounded-t-lg">
                    <div class="bg-white border-b border-gray-200 py-6 px-4 sm:px-6 lg:px-8">
                        <h2 class="text-lg font-medium text-gray-900">
                            URL: <span class="font-normal text-indigo-400 cursor-default prevent-select" onclick="navigator.clipboard.writeText(this.getAttribute('clipboard'));this.style.color = '#30a730';" clipboard="nfc.mhl.sk/card/{{Auth::id()}}">https://nfc.mhl.sk/card/{{Auth::id()}}</span>
                        </h2>
                    </div>
                </div>
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-b-lg">
                    <div class="bg-white border-b border-gray-200 py-6 px-4 sm:px-6 lg:px-8">
                        <header class="overflow-auto">
                            <div class="float-left">
                                <h2 class="text-lg font-medium text-gray-900">
                                    Setup process
                                </h2>
                                <p class="mt-1 text-sm text-gray-600">
                                    Configuring your new card
                                </p>
                            </div>
                        </header>
                        <div class="mt-6">
                            <div class="md:columns-2">
                                <div>
                                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/e_j_OPDnqT0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                    <a href="https://apps.apple.com/us/app/nfc-tools/id1252962749" class="inline-block mt-5" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-apple inline-block" viewBox="0 0 16 16">
                                            <path d="M11.182.008C11.148-.03 9.923.023 8.857 1.18c-1.066 1.156-.902 2.482-.878 2.516.024.034 1.52.087 2.475-1.258.955-1.345.762-2.391.728-2.43Zm3.314 11.733c-.048-.096-2.325-1.234-2.113-3.422.212-2.189 1.675-2.789 1.698-2.854.023-.065-.597-.79-1.254-1.157a3.692 3.692 0 0 0-1.563-.434c-.108-.003-.483-.095-1.254.116-.508.139-1.653.589-1.968.607-.316.018-1.256-.522-2.267-.665-.647-.125-1.333.131-1.824.328-.49.196-1.422.754-2.074 2.237-.652 1.482-.311 3.83-.067 4.56.244.729.625 1.924 1.273 2.796.576.984 1.34 1.667 1.659 1.899.319.232 1.219.386 1.843.067.502-.308 1.408-.485 1.766-.472.357.013 1.061.154 1.782.539.571.197 1.111.115 1.652-.105.541-.221 1.324-1.059 2.238-2.758.347-.79.505-1.217.473-1.282Z"/>
                                            <path d="M11.182.008C11.148-.03 9.923.023 8.857 1.18c-1.066 1.156-.902 2.482-.878 2.516.024.034 1.52.087 2.475-1.258.955-1.345.762-2.391.728-2.43Zm3.314 11.733c-.048-.096-2.325-1.234-2.113-3.422.212-2.189 1.675-2.789 1.698-2.854.023-.065-.597-.79-1.254-1.157a3.692 3.692 0 0 0-1.563-.434c-.108-.003-.483-.095-1.254.116-.508.139-1.653.589-1.968.607-.316.018-1.256-.522-2.267-.665-.647-.125-1.333.131-1.824.328-.49.196-1.422.754-2.074 2.237-.652 1.482-.311 3.83-.067 4.56.244.729.625 1.924 1.273 2.796.576.984 1.34 1.667 1.659 1.899.319.232 1.219.386 1.843.067.502-.308 1.408-.485 1.766-.472.357.013 1.061.154 1.782.539.571.197 1.111.115 1.652-.105.541-.221 1.324-1.059 2.238-2.758.347-.79.505-1.217.473-1.282Z"/>
                                        </svg>
                                        <span class="inline-block ml-3">
                                        Download NFC Tools for iOS
                                    </span>
                                    </a>
                                </div>
                                <div>
                                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/_f67FyfH4Qk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" class="mt-5" allowfullscreen></iframe>
                                    <a href="https://play.google.com/store/apps/details?id=com.wakdev.wdnfc&hl=sk&gl=US" class="inline-block mt-5" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-google-play inline-block" viewBox="0 0 16 16">
                                            <path d="M14.222 9.374c1.037-.61 1.037-2.137 0-2.748L11.528 5.04 8.32 8l3.207 2.96 2.694-1.586Zm-3.595 2.116L7.583 8.68 1.03 14.73c.201 1.029 1.36 1.61 2.303 1.055l7.294-4.295ZM1 13.396V2.603L6.846 8 1 13.396ZM1.03 1.27l6.553 6.05 3.044-2.81L3.333.215C2.39-.341 1.231.24 1.03 1.27Z"/>
                                        </svg>
                                        <span class="inline-block ml-3">
                                        Download NFC Tools for Android
                                    </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</x-app-layout>
