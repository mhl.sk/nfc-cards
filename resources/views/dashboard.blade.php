<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add your contact details') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 ">
            <div class="bg-white overflow-hidden sm:rounded-lg shadow">
                <div class="bg-white border-b border-gray-200 py-6 px-4 sm:px-6 lg:px-8">
                    <header>
                        <h2 class="text-lg font-medium text-gray-900">
                            My contact details
                        </h2>
                        <p class="mt-1 text-sm text-gray-600">
                            You can add all your contact details here. Empty fields will be ignored.
                        </p>
                    </header>
                    <x-splade-form action="/contacts/update" method="patch" default="{{$stored}}">
                        @foreach($services as $service)
                            <div class="mt-6">
                                <x-splade-input
                                    name="{{$service->name}}"
                                    label="{{$service->label}}"
                                    v-model="form.{{$service->name}}"
                                />
                            </div>
                        @endforeach
                        <div  class="mt-6">
                            <x-splade-submit />
                            @if (session('status') === 'contacts-updated')
                                <p class="text-sm text-gray-600 inline pl-3">
                                    {{ __('Saved') }}
                                </p>
                            @endif
                        </div>
                    </x-splade-form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
