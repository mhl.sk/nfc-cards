<!doctype html>
<html lang="en">
<head>

    <!-- Title -->
    <title>{{$name}} - Digital Business Card</title>

    <!-- Meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="/favicon.png" />

    <!-- Scripts -->
    @vite(['resources/js/app.js'])

</head>
<body class="min-h-screen bg-gray-100">
    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">{{$name}}</h2>
            <p>Digital Business Card</p>
        </div>
    </header>

    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="bg-white border-b border-gray-200 rounded-lg shadow">
            @foreach($services as $key => $service)
                @php
                    $service_details = \App\Models\Service::findOrFail($service->service_id);
                @endphp
                <div class="p-6 @if($key != count($services) - 1) border-b @endif flex items-center place-content-between">
                    <div>
                        <b class="block">
                            {{$service_details->label}}:
                        </b>
                        @if($service_details->name == "email")
                            <a href="mailto:{{$service->link}}" target="_blank">
                                {{$service->link}}
                            </a>
                        @elseif($service_details->name == "phone")
                            <a href="tel:{{$service->link}}" target="_blank">
                                {{$service->link}}
                            </a>
                        @else
                            <a href="{{$service->link}}" target="_blank">
                                {{$service->link}}
                            </a>
                        @endif
                    </div>
                    <div class="float-right cursor-pointer" onclick="navigator.clipboard.writeText(this.getAttribute('clipboard'));document.getElementById('copied').style.display='block';setTimeout(function(){document.getElementById('copied').style.display='none'}, 3000)" clipboard="{{$service->link}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard" viewBox="0 0 16 16">
                            <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"/>
                            <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"/>
                        </svg>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div id="copied" class="hidden bg-indigo-500">
        Copied to your clipboard
    </div>
</body>
</html>
