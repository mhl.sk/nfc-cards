<?php

namespace App\Http\Controllers;

use App\Models\InputType;
use App\Models\Service;
use App\Models\UsersServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ContactsController extends Controller
{

    public function update(Request $request)
    {

        /** Delete his previous contacts details */
        UsersServices::where("user_id", Auth::id())->delete();

        /** Add new ones */
        foreach (Service::all() as $service) {
            if(isset($request->{$service->name})) {

                /** Add validator property */
                $valid = false;

                /** Get service type */
                $service_type = InputType::findOrFail($service->type_id)->name;

                /** Do the validation here */
                if($service_type == "url") {
                    if(filter_var($request->{$service->name},FILTER_VALIDATE_URL)) {
                        $valid = true;
                    }
                } else if($service_type == "email") {
                    if(filter_var($request->{$service->name},FILTER_VALIDATE_EMAIL)) {
                        $valid = true;
                    }
                } else {
                    $valid = true;
                }

                /** If input is valid */
                if($valid) {

                    /** Insert it to DB */
                    $add = new UsersServices();
                    $add->user_id = Auth::id();
                    $add->service_id = $service->id;
                    $add->link = $request->{$service->name};
                    $add->save();

                }

            }
        }



        return Redirect::route('dashboard')->with('status', 'contacts-updated');

    }

}
