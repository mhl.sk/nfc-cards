<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UsersServices;
use Illuminate\Http\Request;
use ProtoneMedia\Splade\Facades\SEO;

class CardController extends Controller
{

    public function show($id)
    {

        $user = User::findOrFail($id);
        $services = UsersServices::where("user_id", $id)->get();

        SEO::title($user->name . ' - NFC Card')
            ->description('Digital business card of ' . $user->name)
            ->keywords('digital business card');

        return view('card')->withName($user->name)->withServices($services);
    }

}
