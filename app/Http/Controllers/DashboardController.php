<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\UsersServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function show()
    {
        $stored = [];
        foreach (UsersServices::where("user_id", Auth::id())->get() as $service) {
            $stored[Service::where("id", $service->service_id)->first()->name] = $service->link;
        }
        return view('dashboard')
            ->withServices(Service::all()->sortBy("name"))
            ->withStored(json_encode($stored))
            ;
    }
}
